#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 10:28:49 2020

@author: sean
"""

# https://dash.plotly.com/dash-core-components
# https://dash-bootstrap-components.opensource.faculty.ai/
# Data source used in this example is from https://www.kaggle.com/dheerajmpai/hospitals-and-beds-in-india?select=Hospitals_and_Beds_statewise.csv

# Key dependencies
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State

import base64
import io
import pandas as pd
import plotly.express as px

# CSS stylesheet
#external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# Initialise Dash app object
#app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app = dash.Dash(__name__)

# The app layout
app.layout = html.Div(children=[
    html.Div(className='padsection color-section', children=[
        html.Div(className='container', children=[
            html.H1(children=['My first web app']),
            ]),
        ]),
    html.Div(className='padsection', children=[
        html.Div(className='b-container', children=[
            html.P(children=["In this web app we are going to upload a dataset and look at different views of it"]),
            html.P(children=["There is a lot of help available online to get you started using Dash. The two most useful links are ",
                         html.A(href="https://dash.plotly.com/dash-core-components", target="_blank", children=["https://dash.plotly.com/dash-core-components"]),
                         " and ",
                         html.A(href="https://dash-bootstrap-components.opensource.faculty.ai/", target="_blank", children=["https://dash-bootstrap-components.opensource.faculty.ai/"])
                ]),
            ]),
        ]),
    html.Div(className='padsection color-section', children=[
        html.Div(className='row', children=[
            html.Div(className='b-container', children=[
                dcc.Upload(id='upload_obj', className='upload-obj obj-center', children=[html.Div(children=['Drag and Drop or ', html.A('Select Files')])]),
                ]),
            ]),
        ]),
    html.Div(className='padsection', children=[
        html.Div(className='b-container', children=[
            html.H3("Uploaded data summary"),
            ]),
        html.Div(className='b-container', children=[
            html.Div(className='row', children=[
                html.P(children=["Below is a summary of the data we have uploaded. The callback is automatically triggered when the data is uploaded."]),
                ]),
            ]),
        html.Div(className='b-container', children=[
            html.Div(className='row', children=[
                html.Div(id="summary_table_container"),
                ]),
            ]),
        ]),
    html.Div(className='padsection color-section', children=[
        html.Div(className='b-container', children=[
            html.H3("Interactive graph"),
            ]),
        html.Div(className='b-container', children=[
            html.Div(className='row', children=[
                html.Div(id="graph_container", className='graph', children=[
                    dcc.Graph(id="graph")
                    ]),
                ]),
            ]),
        html.Div(className='b-container', children=[
            html.H4("Select data subset")
            ]),
        html.Div(className='b-container', children=[
            html.Div(className='row', children=[
                html.Div(id="opts_container", className="graph-opts", children=[
                    dcc.Dropdown(id="opts_drop"),
                    dcc.Store(id="data_store")
                    ]),
                ]),
            ]),
        ]),
    html.Div(className='padsection', children=[
        html.Div(className='b-container', children=[
            html.P(children=["The data for this example is provided courtesy of ",
                             html.A(href="https://www.kaggle.com/dheerajmpai/hospitals-and-beds-in-india?select=Hospitals_and_Beds_statewise.csv",
                                    target="_blank", children=["https://www.kaggle.com/dheerajmpai/hospitals-and-beds-in-india?select=Hospitals_and_Beds_statewise.csv"]),
                ]),
            ]),
        ]),
    ])

def import_contents(contents, filename):
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            df = pd.read_csv(io.StringIO(decoded.decode('utf-8')), index_col=0)
        elif 'xls' in filename:
            df = pd.read_excel(io.BytesIO(decoded))
    except Exception as e:
        print(e)
        return None
    return df

def create_summary_table(df):
    if df is not None:
        names = list(df.columns)
        means = df.mean(axis=0)
        median = df.median(axis=0)
        stdDev = df.std(axis=0)
        n = df.count(axis=0)
        minimum = df.min(axis=0)
        maximum = df.max(axis=0)
        table = pd.DataFrame({'Column name': names, 'n': n, 'Mean': means, 'Median': median, 'Std Deviation': stdDev, 'Minimum': minimum, 'Maximum': maximum})
        summary_table = dbc.Table.from_dataframe(table, striped=True, bordered=True)
        return summary_table

def gen_options(df):
    if df is not None:
        names = list(df.columns)
        long_names = ["Primary health centers", "Community health centers", "Sub-District/Divisional Hospitals", "District Hospitals", "Total number of beds"]
        options = []
        for i in range(len(names)):
            options.append({'label': long_names[i], 'value': names[i]})
        return options

def get_title(options, opt):
    if opt is not None:
        a = list(filter(lambda sub: sub['value'] == opt, options))
        b = a[0]['label']
        return b
    else:
        b = "Primary health centers"
        return b

def create_graph(df, opt, title):
    if opt is not None:
        fig = px.bar(df, x=df.index, y=opt, color=opt, title=title)
        graph_obj = dcc.Graph(id="graph", figure=fig)
        return graph_obj
    else:
        fig = px.bar(df, x=df.index, y='PHC', color='PHC', title="Primary health centers")
        graph_obj = dcc.Graph(id="graph", figure=fig)
        return graph_obj

@app.callback([Output('summary_table_container', 'children'),
               Output('graph_container', 'children'),
               Output('opts_drop', 'options'),
               Output('data_store', 'data')],
              [Input('upload_obj', 'contents'),
               Input('opts_drop', 'value')],
              [State('upload_obj', 'filename')])
def upload_table_graph(data, opt, filename):
    if data is not None:
        df = import_contents(data, filename)
        if df is not None:
            summary_table = create_summary_table(df)
            options = gen_options(df)
            title = get_title(options, opt)
            graph = create_graph(df, opt, title)
            df_dict = df.to_dict()
        return summary_table, graph, options, df_dict

# The server run command
if __name__ == '__main__':
    app.run_server(debug=True)