# An introduction to python Dash

## Set-up instructions
First download or clone this repository<br>
Either set-up a virtual environment or install the required dependencies

### Setting-up a virtual environment using anaconda
1. Windows -> open anaconda prompt. Max/Linux -> open a terminal
2. Navigate to the 'env' folder
3. run `conda env create -f environment.yml`
4. Activate the environment using `conda activate dash_intro`
5. Install the spyder editor on the environment with `conda install spyder`
6. Start spyder with the command `spyder`

### Files
dash_intro
<pre>── <b>assets</b>
│   └── <b>css</b>
│       └── style.css
├── <b>basic_callbacks_example.py</b>
├── <b>basic_layout_example.py</b>
├── <b>basic_styling_example.py</b>
├── dash_intro_presentation.pdf
├── datasets_573696_1039362_Hospitals_and_Beds_statewise.csv
├── <b>env</b>
│   └── environment.yml
└── README.md
</pre>
