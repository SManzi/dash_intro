#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  9 14:42:41 2020

@author: sean
"""

# https://dash.plotly.com/dash-core-components
# https://dash-bootstrap-components.opensource.faculty.ai/
# Data source used in this example is from https://www.kaggle.com/dheerajmpai/hospitals-and-beds-in-india?select=Hospitals_and_Beds_statewise.csv


# Key dependencies
import dash
import dash_core_components as dcc
import dash_html_components as html

# CSS stylesheet
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# Initialise Dash app object
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# The app layout
app.layout = html.Div(children=[
    html.Div(children=[
        html.H1(children=['My first web app']),
        ]),
    html.Div(children=[
        html.P(children=["In this web app we are going to upload a dataset and look at different views of it"]),
        html.P(children=["There is a lot of help available online to get you started using Dash. The two most useful links are ",
                         html.A(href="https://dash.plotly.com/dash-core-components", target="_blank", children=["https://dash.plotly.com/dash-core-components"]),
                         " and ",
                         html.A(href="https://dash-bootstrap-components.opensource.faculty.ai/", target="_blank", children=["https://dash-bootstrap-components.opensource.faculty.ai/"])
                         ]),
        ]),
    html.Div(children=[
        dcc.Upload(id='upload_obj', children=[html.Div(children=['Drag and Drop or ', html.A('Select Files')])]),    
        ]),
    html.Div(children=[
        html.H3("Uploaded data summary"),
        html.P(children=["Below is a summary of the data we have uploaded. The callback is automatically triggered when the data is uploaded."]),
        html.Div(id="summary_table_container"),
        ]),
    html.Div(children=[
        dcc.Graph(id="graph_obj")
        ]),
    ])

# The server run command
if __name__ == '__main__':
    app.run_server(debug=True)